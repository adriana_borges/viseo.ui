import {} from 'dotenv/config.js';
import moment from 'moment';
import * as prod from '../db/mysql/prod.js';

const QUERY_SELECT_NEW_CASES = `
  select a.*, c.*, d.cidade as JO_cidade, d.estado as JO_estado 
  from trf4_precatorios as a 
  left join requisicoes as b 
    on b.numero = a.TRF4_numPrecatorio
  inner join vis_requerentes as c 
    on a.bot_candidatoCPF = c.pes_documento
  inner join vw_juizo_origem as d 
    on a.TRF4_juizoOrigem = d.juizo_origem
  where a.bot_candidatoCPF is not null 
  and a.bot_CPFValidado = 1
  and a.bot_tipoPDF in ('PETIÇÃO INICIAL1','REQUISIÇÃO DE PAGAMENTO1')
  and b.id is null
  and a.TRF4_valorRequisicao is not null
`;

const QUERY_INSERT_TELEFONE = `
insert into telefones (
  id,
  id_requisicoes,
  numero,
  dt_cadastro
) values (
  NULL,
  ?,
  ?,
  ?
)
`;

const QUERY_INSERT_REQUISICOES = `
insert into requisicoes (
  id,
  documento,
  numero,
  data_protocolo_trf,
  situacao_protocolo,
  oficio_requisitorio,
  juizo_origem,
  processos_originarios,
  requerido,
  requerentes,
  advogado,
  mes_ano_proposta,
  data_conta_liquidacao,
  valor_solicitado,
  valor_inscrito_proposta,
  situacao_proposta,
  banco,
  natureza,
  tipo_causa,
  status,
  cidade,
  estado,
  oab,
  endereco,
  endereco_numero,
  endereco_comp,
  endereco_bairro,
  endereco_cidade,
  endereco_estado,
  endereco_cep,
  tribunal,
  processo
)
values (
NULL
,?
,?
,?
,'REGISTRADA'
,NULL
,?
,?
,?
,?
,?
,?
,?
,?
,?
,?
,'Não informado'
,'ALIMENTICIA'
,?
,1
,?
,?
,?
,?
,?
,?
,?
,?
,?
,?
,'TRF4'
,NULL
)
`;

const FORMATO_DATA_BANCO = 'YYYY-MM-DD';
const FORMATO_DATA_BANCO_TEMPO = 'YYYY-MM-DD HH:mm:ss';

export default async function autobotTRF4(mysqlPoolProd) {
	const [rows] = await mysqlPoolProd.query(QUERY_SELECT_NEW_CASES);

	console.log(`Encontrados ${rows.length} precatorios para inserir.`);
	while (rows.length > 0) {
		let precatorioValidado = rows.pop();

		try {
			let [precatorio] = await mysqlPoolProd.query(QUERY_INSERT_REQUISICOES, [
				precatorioValidado.bot_candidatoCPF,
				precatorioValidado.TRF4_numPrecatorio,
				moment(precatorioValidado.TRF4_dataAutuacao)
					.utc()
					.format(FORMATO_DATA_BANCO_TEMPO),
				precatorioValidado.TRF4_juizoOrigem,
				precatorioValidado.TRF4_numProcessoOriginario,
				precatorioValidado.TRF4_requerido,
				precatorioValidado.TRF4_cedente,
				precatorioValidado.TRF4_nomeAdvogado.substring(0, 149),
				precatorioValidado.TRF4_anoProposta == null
					? moment(precatorioValidado.TRF4_dataAutuacao).month() < 6
						? moment(moment().year() + 1)
								.utc()
								.format(FORMATO_DATA_BANCO)
						: moment(moment().year() + 2)
								.utc()
								.format(FORMATO_DATA_BANCO)
					: moment(precatorioValidado.TRF4_anoProposta)
							.utc()
							.format(FORMATO_DATA_BANCO),
				moment(precatorioValidado.TRF4_dataAutuacao)
					.utc()
					.format(FORMATO_DATA_BANCO_TEMPO),
				precatorioValidado.TRF4_valorRequisicao,
				precatorioValidado.TRF4_valorRequisicao,
				precatorioValidado.bot_tipoPDF,
				precatorioValidado.TRF4_tipoCausa.substring(0, 255),
				precatorioValidado.JO_cidade,
				precatorioValidado.JO_estado,
				precatorioValidado.TRF4_OAB,
				precatorioValidado.end_nome_rua,
				precatorioValidado.end_numero,
				precatorioValidado.end_complemento,
				precatorioValidado.end_bairro,
				precatorioValidado.end_cidade,
				precatorioValidado.end_uf,
				precatorioValidado.end_cep,
			]);

			let id = precatorio.insertId;

			console.log(
				`Inserido precatorio ${precatorioValidado.TRF4_numPrecatorio} com ID ${id}`
			);

			if (precatorioValidado?.tel_lista?.length > 0) {
				let telefones = precatorioValidado.tel_lista.split(',');
				console.log(
					`Inserindo ${telefones.length} telefone(s) [${telefones}] no precatorio de ID ${id}`
				);
				while (telefones.length > 0) {
					let tel = telefones.pop();

					await mysqlPoolProd.query(QUERY_INSERT_TELEFONE, [
						id,
						tel,
						moment().utc().format('YYYY-MM-DD HH:mm:ss'),
					]);
				}
			} else {
				console.log(`Sem telefones para inserir.`);
			}
		} catch (error) {
			console.log(error.sqlMessage, precatorioValidado);
		}
	}
}

await autobotTRF4(prod.default);
process.exit(0);

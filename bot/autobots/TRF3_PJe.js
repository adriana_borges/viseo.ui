import 'dotenv/config.js';
import puppeteer from 'puppeteer';
import axios from 'axios';
import moment from 'moment';
import Sequelize from 'sequelize';
import cheerio from 'cheerio';

const sequelize = new Sequelize(
	process.env.MYSQL_DB,
	process.env.MYSQL_USER,
	process.env.MYSQL_PASS,
	{
		host: process.env.MYSQL_CLUSTER_PROD,
		dialect: 'mysql',
	}
);

const MSG_PREC_NAO_EXISTE =
	'Sua pesquisa não encontrou nenhum processo disponível.';

const USER_AGENT = `Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4464.0 Safari/537.36 Edg/91.0.852.0`;

const TRF3Details = {
	sitekey: '23e3120e-ae2c-4044-a9f9-326518c4332a',
	pageurl: 'https://pje2g.trf3.jus.br/pje/ConsultaPublica/listView.seam',
};

const pageSelectors = {
	input: {
		processo:
			'#fPP\\:numProcesso-inputNumeroProcessoDecoration\\:numProcesso-inputNumeroProcesso',
	},
	button: {
		pesquisar: '#fPP\\:searchProcessos',
	},
};

const captchaOptions = {
	key: process.env.TWOCAPTCHA_API_KEY,
	numeric: 2, // * 2 - captcha contains only letters
	sitekey: TRF3Details.sitekey,
	method: 'hcaptcha',
	userAgent: USER_AGENT,
	header_acao: 1,
	json: 1,
};

let lastID;

process.on('exit', async (code) => {
	console.log(`Exiting with a code: ${code}`);
	await sequelize
		.query(
			`
			UPDATE trf3_precatorios
			SET bot_candidatoCPF = NULL
			,bot_enriquecido = 0
			,bot_ultimaAtualizacao = ?
			,bot_PJe_status = NULL
			WHERE TRF3_numeroProtocolo = ?
	`,
			{
				raw: true,
				replacements: [moment().utc().format('YYYY-MM-DD HH:mm:ss'), lastID],
				type: sequelize.QueryTypes.UPDATE,
			}
		)
		.then(() => {
			console.log('Finalizando...');
			process.exit(0);
		});
});

(async function mainmain() {
	do {
		await main();
	} while (true);
})();

async function main() {
	try {
		console.log({
			level: 'info',
			message: `Saldo do 2Captcha: ${
				(
					await axios.get(
						`https://2captcha.com/res.php?key=${process.env.TWOCAPTCHA_API_KEY}&action=getbalance&json=1`
					)
				).data.request
			}`,
		});
	} catch {}

	const clients = await getCPF(0);

	console.log({
		level: 'info',
		message: `Achei ${clients.length} cliente(s)`,
	});

	if (clients.length === 0) process.exit(0);

	const browser = await puppeteer.launch({
		headless: true,
		// executablePath:
		// 	'C:\\Program Files (x86)\\Microsoft\\Edge\\Application\\msedge.exe',
		slowMo: 50,
		defaultViewport: null,
	});

	while (clients.length >= 1) {
		let client = await clients.pop();

		console.log({
			level: 'info',
			message: `Pesquisando o processo: ${client.TRF3_processo_originario}`,
		});

		lastID = client.TRF3_numeroProtocolo;

		let temPreca = await buscaPrecatoriosTRF3(client, browser);

		console.log(`Retorno do busca ${temPreca?.length} itens:`, temPreca);

		if (temPreca.length === 1 || temPreca.length === 33) {
			if (temPreca === '»\nUnhandled or Wrapper Exception ') {
				console.log({
					level: 'info',
					message: `Erro ao consultar ${client.TRF3_numeroProtocolo} no PJe.`,
				});
				await sequelize.query(
					`
						UPDATE trf3_precatorios
						SET bot_candidatoCPF = NULL
						,bot_enriquecido = 0
						,bot_ultimaAtualizacao = ?
						,bot_PJe_status = 3
						WHERE TRF3_numeroProtocolo = ?
				`,
					{
						raw: true,
						replacements: [
							moment().utc().format('YYYY-MM-DD HH:mm:ss'),
							client.TRF3_numeroProtocolo,
						],
						type: sequelize.QueryTypes.UPDATE,
					}
				);
			} else if (temPreca === '0') {
				console.log({
					level: 'info',
					message: `Precatorio ${client.TRF3_numeroProtocolo} nao está no PJe.`,
				});
				await sequelize.query(
					`
						UPDATE trf3_precatorios
						SET bot_candidatoCPF = NULL
						,bot_enriquecido = 0
						,bot_ultimaAtualizacao = ?
						,bot_PJe_status = 0
						WHERE TRF3_numeroProtocolo = ?
				`,
					{
						raw: true,
						replacements: [
							moment().utc().format('YYYY-MM-DD HH:mm:ss'),
							client.TRF3_numeroProtocolo,
						],
						type: sequelize.QueryTypes.UPDATE,
					}
				);
			} else {
				await sequelize.query(
					` update trf3_precatorios
						set bot_PJe_status = null,
						bot_ultimaAtualizacao = ?
						where TRF3_numeroProtocolo = ?
					`,
					{
						raw: true,
						replacements: [
							moment().utc().format('YYYY-MM-DD HH:mm:ss'),
							client.TRF3_numeroProtocolo,
						],
						type: sequelize.QueryTypes.UPDATE,
					}
				);
			}
		} else {
			console.log('Entrou no caso positivo!!!!!!!!!!!');

			await sequelize.query(
				`
					UPDATE trf3_precatorios
					SET bot_disponibilizado = 0
					,bot_ultimaAtualizacao = ?
					,bot_PJe_status = 1
					WHERE TRF3_numeroProtocolo = ?
			`,
				{
					raw: true,
					replacements: [
						moment().utc().format('YYYY-MM-DD HH:mm:ss'),
						client.TRF3_numeroProtocolo,
					],
					type: sequelize.QueryTypes.UPDATE,
				}
			);

			temPreca.forEach(async (parte) => {
				try {
					await sequelize.query(
						`
							INSERT INTO bot_validacao VALUES (?,1)
					`,
						{
							raw: true,
							replacements: [parte.documento.replaceAll(/([\/.-])/g, '')],
							type: sequelize.QueryTypes.UPDATE,
						}
					);
				} catch (error) {
					console.log(error.sqlMessage);
				}
				try {
					await sequelize.query(
						`
						insert into pje_partes (TRF3_processo_originario, nome, documento, oab, dt_insert) values (?,?,?,?,?)
				`,
						{
							raw: true,
							replacements: [
								client.TRF3_processo_originario,
								parte.nome,
								parte.documento.replaceAll(/([\/.-])/g, ''),
								parte.OAB || 'NULL',
								moment().utc().format('YYYY-MM-DD HH:mm:ss'),
							],
							type: sequelize.QueryTypes.UPDATE,
						}
					);
				} catch (error) {
					console.log(error);
				}
			});
		}
	}

	browser.close();
}

async function getCPF() {
	let cand = await sequelize.query(
		`	select a.TRF3_numeroProtocolo, a.TRF3_processo_originario 
			from trf3_precatorios as a 
			where a.TRF3_procedimento = 'PRC' 
				and a.TRF3_ano_proposta >= '2022-01-01' 
				and (a.TRF3_valor_inscrito_proposta >= 63000 OR a.TRF3_valor_solicitado >= 63000)
				and a.bot_disponibilizado = 0 
				and a.bot_enriquecido = 0
				and a.bot_PJe_status is null 
				and a.bot_candidatoCPF is null 
				and a.TRF3_processo_originario like '%4.03%' 
			limit 1`,
		{
			raw: true,
			replacements: [],
			type: sequelize.QueryTypes.SELECT,
		}
	);

	if (cand.length > 0) {
		await sequelize.query(
			` update trf3_precatorios
				set bot_PJe_status = -1,
				bot_ultimaAtualizacao = ?
				where TRF3_numeroProtocolo = ?
			`,
			{
				raw: true,
				replacements: [
					moment().utc().format('YYYY-MM-DD HH:mm:ss'),
					cand[0].TRF3_numeroProtocolo,
				],
				type: sequelize.QueryTypes.UPDATE,
			}
		);
	}

	return cand;
}

async function buscaPrecatoriosTRF3(client, bw) {
	let page = await bw.newPage();

	page.setUserAgent(USER_AGENT); //Muda o User Agent para poder usar o headless

	await page.goto(TRF3Details.pageurl, { waitUntil: 'networkidle0' });

	let requestId;
	// do {
	// 	try {
	// 		requestId = (
	// 			await axios.post(`https://2captcha.com/in.php`, {
	// 				...captchaOptions,
	// 				pageurl: page.url(),
	// 			})
	// 		).data.request;
	// 	} catch {}

	// 	await timeout(5_000);
	// } while (requestId === 'ERROR_NO_SLOT_AVAILABLE');

	// console.log({
	// 	level: 'debug',
	// 	message: `RequestID: ${requestId}`,
	// });

	await page.type(
		pageSelectors.input.processo,
		client.TRF3_processo_originario
	);

	let ck = await page.cookies();

	ck.forEach(async (cookie) => {
		if (cookie.name === 'RT' || cookie.name === 'ak_bmsc') {
			await page.deleteCookie(cookie);
		}
	});

	let response;
	// do {
	// 	try {
	// 		response = await axios.get(
	// 			`https://2captcha.com/res.php?key=${process.env.TWOCAPTCHA_API_KEY}&action=get&id=${requestId}&json=1`
	// 		);

	// 		if (response?.data.status != 1) {
	// 			console.log({
	// 				level: 'debug',
	// 				message: `${JSON.stringify(response.data)}`,
	// 			});
	// 		}

	// 		await timeout(5_000);

	// 		if (response?.data.request === 'ERROR_CAPTCHA_UNSOLVABLE') {
	// 			return '1';
	// 		}
	// 	} catch {}
	// } while (response?.data.status != 1);

	try {
		// console.log('agora');

		await page.evaluate(async (token, MSG_PREC_NAO_EXISTE) => {
			try {
				PJeOffice.verificarDisponibilidade = function () {};
			} catch (error) {
				console.log(error);
			}

			// document
			// 	.querySelector('[name=g-recaptcha-response]')
			// 	.removeAttribute('style');

			// document
			// 	.querySelector('[name=h-captcha-response]')
			// 	.removeAttribute('style');

			// document.querySelector('[name=g-recaptcha-response]').innerText = token;
			// document.querySelector('[name=h-captcha-response]').innerText = token;

			onSubmit(token);
		}, response?.data?.request);

		// await timeout(100_000);

		await Promise.race([
			page.waitForSelector('#fPP\\:j_id218 > dt > span'),
			page.waitForSelector('#fPP\\:processosTable\\:tb >tr >td>a'),
		]);

		let result = await page
			.evaluate(() => {
				let mg = document.querySelector('#fPP\\:j_id218 > dt > span');
				let vr = document.querySelector('#fPP\\:processosTable\\:tb >tr >td>a');

				if (mg != null) {
					return mg.innerText;
				} else if (vr != null) {
					return vr.getAttribute('onClick');
				}
			})
			.then(async (val) => {
				if (val === MSG_PREC_NAO_EXISTE) {
					console.log(
						'O precatório não existe np PJE... Fazer alguma coisa com essa info...'
					);
					await page.close();

					return '0';
				} else if (val === 'Unhandled or Wrapper Exception ') {
					await page.close();
					return 'Exception';
				} else {
					let m = val.split(`'`);

					console.log('Link: ', `https://pje2g.trf3.jus.br${m[3]}`);

					await page.goto(`https://pje2g.trf3.jus.br${m[3]}`, {
						waitUntil: 'networkidle0',
					});

					await Promise.race([
						page.waitForSelector(
							'#j_id130\\:processoPartesPoloAtivoResumidoList\\:tb'
						),
						page.waitForSelector('#j_id122_header'),
					]);

					let partes = await page.evaluate(() => {
						let timeout = document.querySelector('#j_id122_header');
						if (timeout != null) {
							return timeout.innerText;
						}
						let partes = document.querySelectorAll('td > span > div > span');
						let tempPartes = [];
						partes.forEach((pt) => {
							/* 
								Steps{
									1. remove a palavra entre parenteses ex: (ADVOGADO), (EXEQUENTE), (APELANTE), (APELADO) usando o regex /( \(\w+\))/gi
									2. cria um separador no campo CPF/CNPJ
									3. cria se necessário um separador no campo OAB
									4. divide a string pelo separador '|'
									5. cria um objeto com os parametros necessários e coloca no array tempPartes
								}
							*/
							let tempWord = pt.innerText
								.replace(/( \([\w|\w \w]+\))/gi, '')
								.replace(/( - CPF: | - CNPJ: )/gi, '|')
								.replace(/( - OAB )/gi, '|')
								.split('|');

							if (tempWord.length > 2) {
								tempPartes.push({
									nome: tempWord[0],
									OAB: tempWord[1],
									documento: tempWord[2],
								});
							} else {
								tempPartes.push({ nome: tempWord[0], documento: tempWord[1] });
							}
							tempWord = '';
						});
						return tempPartes;
					});

					await page.close();

					return partes;
				}
			});

		return result;
	} catch (error) {
		console.log(error);
		await sequelize.query(
			` update trf3_precatorios
				set bot_PJe_status = null,
				bot_ultimaAtualizacao = ?
				where TRF3_numeroProtocolo = ?
			`,
			{
				raw: true,
				replacements: [
					moment().utc().format('YYYY-MM-DD HH:mm:ss'),
					client.TRF3_numeroProtocolo,
				],
				type: sequelize.QueryTypes.UPDATE,
			}
		);
		process.exit(1);
	}
}

async function initiateCaptchaRequest(apiKey) {
	const response = await axios.post('http://2captcha.com/in.php', formData);
	return response.data.request;
}

const timeout = (millis) =>
	new Promise((resolve) => setTimeout(resolve, millis));

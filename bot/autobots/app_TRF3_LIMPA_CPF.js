import 'dotenv/config.js';
import puppeteer from 'puppeteer';
import axios from 'axios';
import moment from 'moment';
import Sequelize from 'sequelize';

const sequelize = new Sequelize(
	process.env.MYSQL_DB,
	process.env.MYSQL_USER,
	process.env.MYSQL_PASS,
	{
		host: process.env.MYSQL_CLUSTER_PROD,
		dialect: 'mysql',
	}
);

const TRF3Details = {
	sitekey: '6LcMoT0aAAAAAL36YkPdesuSXnXQgmVSc-cmnvU6',
	pageurl: 'https://web3.trf3.jus.br/consultas/Internet/ConsultaReqPag',
};

const captchaAPI = 'ff6bfa1e675a0d74cc23a162f733b2d7';

const pageSelectors = {
	input: {
		cpf: '#CpfCnpj',
	},
	button: {
		pesquisar: '#pesquisar',
	},
};

const MSG_SEM_PRECATORIOS_NO_CPF =
	'Não há ofícios que atendem aos critérios de pesquisa.';

(async function mainmain() {
	do {
		await main();
	} while (true);
})();

async function main() {
	console.log({
		level: 'info',
		message: `Saldo do 2Captcha: ${
			(
				await axios.get(
					`https://2captcha.com/res.php?key=${captchaAPI}&action=getbalance&json=1`
				)
			).data.request
		}`,
	});

	const clients = await getCPF(0);

	console.log({
		level: 'info',
		message: `Achei ${clients.length} cliente(s)`,
	});

	if (clients.length === 0) process.exit(0);

	const browser = await puppeteer.launch({
		headless: true,
		// executablePath: '/user/bin/chromium/',
		slowMo: 50,
		defaultViewport: null,
	});

	while (clients.length >= 1) {
		let client = await clients.pop();

		const page = await browser.newPage();

		page.setUserAgent(
			'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4464.0 Safari/537.36 Edg/91.0.852.0'
		); //Muda o User Agent para poder usar o headless

		console.log({
			level: 'info',
			message: `Pesquisando cliente com o CPF: ${client.bot_candidatoCPF}`,
		});

		await sequelize.query(
			`
				UPDATE viseo.trf3_precatorios
				SET bot_ultimaAtualizacao = ?
				WHERE TRF3_numeroProtocolo = ?
		`,
			{
				raw: true,
				replacements: [
					moment().utc().format('YYYY-MM-DD HH:mm:ss'),
					client.TRF3_numeroProtocolo,
				],
				type: sequelize.QueryTypes.UPDATE,
			}
		);

		let temPreca = await buscaPrecatoriosTRF3(client, page);

		temPreca = [...temPreca]; // quick fix pra transformar os items em um array.

		console.log({
			level: 'info',
			message: `O cliente ${client.bot_candidatoCPF} ${
				temPreca[0] === 0 ? 'não tem' : 'tem'
			} precatório`,
		});

		console.log({
			level: 'info',
			message: `${temPreca}`,
		});

		if (temPreca[0] === 0) {
			console.log({
				level: 'info',
				message: `Removendo o CPF ${client.bot_candidatoCPF} do precatorio ${client.TRF3_numeroProtocolo}`,
			});

			await sequelize.query(
				`
					UPDATE viseo.trf3_precatorios
					SET bot_candidatoCPF = NULL
					,bot_enriquecido = 0
					,bot_ultimaAtualizacao = ?
					WHERE TRF3_numeroProtocolo = ?
			`,
				{
					raw: true,
					replacements: [
						moment().utc().format('YYYY-MM-DD HH:mm:ss'),
						client.TRF3_numeroProtocolo,
					],
					type: sequelize.QueryTypes.UPDATE,
				}
			);
			try {
				await sequelize.query(
					`
							INSERT INTO viseo.bot_validacao VALUES (?,0)
					`,
					{
						raw: true,
						replacements: [client.bot_candidatoCPF],
						type: sequelize.QueryTypes.UPDATE,
					}
				);
			} catch (error) {}
		} else {
			try {
				await sequelize.query(
					`
							INSERT INTO viseo.bot_validacao VALUES (?,1)
					`,
					{
						raw: true,
						replacements: [client.bot_candidatoCPF],
						type: sequelize.QueryTypes.UPDATE,
					}
				);
			} catch (error) {}

			if ([...temPreca].indexOf(client.TRF3_numeroProtocolo) >= 0) {
				await sequelize.query(
					`
						UPDATE viseo.trf3_precatorios
						SET bot_disponibilizado = 1
						,bot_ultimaAtualizacao = ?
						WHERE TRF3_numeroProtocolo = ?
				`,
					{
						raw: true,
						replacements: [
							moment().utc().format('YYYY-MM-DD HH:mm:ss'),
							client.TRF3_numeroProtocolo,
						],
						type: sequelize.QueryTypes.UPDATE,
					}
				);
			} else {
				await sequelize.query(
					`
						UPDATE viseo.trf3_precatorios
						SET bot_candidatoCPF = NULL
						,bot_enriquecido = 0
						,bot_ultimaAtualizacao = ?
						WHERE TRF3_numeroProtocolo = ?
				`,
					{
						raw: true,
						replacements: [
							moment().utc().format('YYYY-MM-DD HH:mm:ss'),
							client.TRF3_numeroProtocolo,
						],
						type: sequelize.QueryTypes.UPDATE,
					}
				);
			}
		}
	}

	browser.close();
}

async function getCPF() {
	return await sequelize.query(
		`select bot_candidatoCPF, TRF3_numeroProtocolo from viseo.trf3_precatorios as a where a.bot_enriquecido = 1 and bot_ultimaAtualizacao is null limit 1`,
		{
			raw: true,
			replacements: [],
			type: sequelize.QueryTypes.SELECT,
		}
	);
}

async function buscaPrecatoriosTRF3(client, page) {
	let temPrecatorio = [];

	await page.goto(TRF3Details.pageurl);

	let requestId;
	do {
		requestId = (
			await axios.post(
				`https://2captcha.com/in.php?key=${captchaAPI}&method=userrecaptcha&googlekey=${TRF3Details.sitekey}&pageurl=${TRF3Details.pageurl}&json=1`
			)
		).data.request;

		await timeout(5000);
	} while (requestId === 'ERROR_NO_SLOT_AVAILABLE');

	console.log({
		level: 'debug',
		message: `RequestID: ${requestId}`,
	});

	await page.evaluate(
		`document.getElementById("pesquisar").removeAttribute("disabled")`
	);

	await page.type(pageSelectors.input.cpf, client.bot_candidatoCPF);

	await timeout(15000);

	let response;
	do {
		response = await axios.get(
			`https://2captcha.com/res.php?key=${captchaAPI}&action=get&id=${requestId}&json=1`
		);

		console.log({
			level: 'debug',
			message: `${JSON.stringify(response.data)}`,
		});

		await timeout(10000);

		if (response.data.request === 'ERROR_CAPTCHA_UNSOLVABLE') {
			break;
		}
	} while (response.data.status != 1);

	await page.evaluate(
		`document.getElementById("g-recaptcha-response").innerHTML="${response.data.request}";`
	);

	await page.click(pageSelectors.button.pesquisar);

	await timeout(500);

	let msgTRF3 = await page.evaluate(() => {
		let lab = document.querySelector('label').innerText;

		return lab;
	});

	let listPrecas = await page.evaluate(() => {
		let precas = [];

		document.querySelectorAll('tr td:nth-child(2)').forEach((e) => {
			precas.push(e.innerText);
		});
		return precas;
	});

	console.log({
		level: 'info',
		message: `${msgTRF3.toString()}`,
	});

	if (msgTRF3 === MSG_SEM_PRECATORIOS_NO_CPF) {
		temPrecatorio.push(0);
	} else {
		listPrecas.forEach((e) => temPrecatorio.push(e));
	}

	await page.close();

	return temPrecatorio;
}

async function initiateCaptchaRequest(apiKey) {
	const response = await axios.post('http://2captcha.com/in.php', formData);
	return response.data.request;
}

const timeout = (millis) =>
	new Promise((resolve) => setTimeout(resolve, millis));

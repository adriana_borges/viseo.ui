import 'dotenv/config.js';
import geraPrecatorio from '../generators/TRF4.js';
import puppeteer from 'puppeteer';
import axios from 'axios';
import cheerio from 'cheerio';
import moment from 'moment';
import aws from 'aws-sdk';
import mysqlPool from '../db/mysql/prod.js';

const options = {
	Bucket: process.env.BUCKET,
	region: process.env.REGION,
	signatureVersion: 'v4',
	ACL: 'public-read',
};
let s3 = new aws.S3(options);

const BASE_URL = `https://www2.trf4.jus.br/trf4/controlador.php`;

const USER_AGENT =
	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36 Edg/92.0.902.62';

const WORDLIST_CEDENTE = [
	'advogados',
	'associados',
	'advocacia',
	'sociedade',
	'paese',
	'individual',
];

const timeout = (millis) =>
	new Promise((resolve) => setTimeout(resolve, millis));

// const captchaOptions = {
// 	key: process.env.TWOCAPTCHA_API_KEY,
// 	sitekey: process.env.TRF4_HCAPTCHA_SITE_KEY,
// 	method: 'hcaptcha',
// 	userAgent: USER_AGENT,
// 	header_acao: 1,
// 	json: 1,
// };

const captchaOptions = {
	key: process.env.TWOCAPTCHA_API_KEY,
	numeric: 2, //2 - captcha contains only letters
	method: 'base64',
	min_len: 6,
	max_len: 6,
	language: 2, //2 - Latin captcha
	header_acao: 1, //1 - enabled. If enabled in.php will include Access-Control-Allow-Origin:* header in the response. Used for cross-domain AJAX requests in web applications.
	json: 1,
};

const respOptions = {
	key: process.env.TWOCAPTCHA_API_KEY,
	action: 'get',
	json: 1,
};

let params = {
	acao: 'consulta_processual_resultado_pesquisa',
	selForma: 'NC', //* 'NU' para consultas simples e 'NC' para consultas com CPF
	selOrigem: 'TRF', //* Origem dos dados
	todasfases: 'S', //* Mostra todas as fases do processo
	todaspartes: 'S', //* Mostra o nome de todos os advogados no processo
	txtValor: undefined, //* Numero do precatório
};

export default async function autobotTRF4(
	mysqlPool,
	QTD_PROCESSOS_TRF4 = 99,
	ANO = '2020' || moment().year()
) {
	console.time(`${QTD_PROCESSOS_TRF4} precatórios - tempo total`);
	const [rows] = await mysqlPool.query(
		`select numero from viseo.requisicoes where tribunal = 'TRF4'
		union all
		select TRF4_numPrecatorio from viseo.trf4_precatorios
		union all
		select TRF4_numPrecatorio from viseo.trf4_precatorios_rpv
		`
	);

	const gen = geraPrecatorio({
		NUMERO: 1,
		ANO: ANO,
		REJECT: rows.map((item) => item.numero),
	});

	const browser = await puppeteer.launch({
		headless: true,
		slowMo: 100,
		defaultViewport: null,
	});

	for (let i = 0; i < QTD_PROCESSOS_TRF4; i++) {
		let doAgain;
		let numPrecatorioGerado;
		let dados;
		do {
			numPrecatorioGerado = gen.next().value;

			console.time(`${numPrecatorioGerado} - tempo total`);
			console.time(`${numPrecatorioGerado} - busca de dados`);
			console.log(numPrecatorioGerado);
			dados = await buscaDados(
				numPrecatorioGerado,
				undefined,
				params.txtPalavraGerada,
				params.hdnRefId,
				browser
			);
			if (dados.TRF4_numPrecatorio == 'Consulta Proc') {
				doAgain = true;
				params.hdnRefId = undefined;
				params.txtPalavraGerada = undefined;
			} else {
				doAgain = false;
			}
		} while (doAgain);
		console.timeEnd(`${numPrecatorioGerado} - busca de dados`);
		console.time(`${numPrecatorioGerado} - processamento de dados`);
		let precaData = trataHTMLLixo(dados);
		console.timeEnd(`${numPrecatorioGerado} - processamento de dados`);

		console.time(`${numPrecatorioGerado} - insert no banco`);

		if (precaData.TRF4_valorCausa == 0 || precaData.TRF4_valorCausa >= 66000) {
			console.log('PRC!');
		}

		try {
			await mysqlPool.query(
				`insert into ${
					precaData.TRF4_valorCausa == 0 || precaData.TRF4_valorCausa >= 66000
						? 'viseo.trf4_precatorios'
						: 'viseo.trf4_precatorios_rpv'
				} (
					 TRF4_numPrecatorio,
					 TRF4_numProcessoOriginario,
					 TRF4_dataAutuacao,
					 TRF4_situacao,
					 TRF4_tipoCompetencia,
					 TRF4_tipoCausa,
					 TRF4_cedente,
					 TRF4_requerido,
					 TRF4_juizoOrigem,
					 TRF4_nomeAdvogado,
					 TRF4_dataUltimaAtualizacao,
					 TRF4_textoUltimaAtualizacao,
					 TRF4_anoProposta,
					 TRF4_tutela,
					 TRF4_justicaGratuita,
					 TRF4_valorCausa,
					 TRF4_intervencaoMP,
					 TRF4_maior60Anos)
				values (
					  '${precaData.TRF4_numPrecatorio}'
					, '${precaData.TRF4_numProcessoOriginario}'
					, '${precaData.TRF4_dataAutuacao}'
					, '${precaData.TRF4_situacao}'
					, '${precaData.TRF4_tipoCompetencia}'
					, '${precaData.TRF4_tipoCausa}'
					, '${precaData.TRF4_cedente}'
					, '${precaData.TRF4_requerido}'
					, '${precaData.TRF4_juizoOrigem}'
					, '${precaData.TRF4_nomeAdvogado}'
					, '${precaData.TRF4_dataUltimaAtualizacao}'
					, '${precaData.TRF4_textoUltimaAtualizacao}'
					,  ${
						precaData.TRF4_anoProposta === null
							? 'NULL'
							: "'" + precaData.TRF4_anoProposta + "'"
					}
					,  ${
						precaData.TRF4_tutela != undefined
							? "'" + precaData.TRF4_tutela + "'"
							: 'NULL'
					}
					,  ${
						precaData.TRF4_justicaGratuita != undefined
							? "'" + precaData.TRF4_justicaGratuita + "'"
							: 'NULL'
					}
					,  ${
						precaData.TRF4_valorCausa != undefined
							? "'" + precaData.TRF4_valorCausa + "'"
							: 'NULL'
					}
					,  ${
						precaData.TRF4_intervencaoMP != undefined
							? "'" + precaData.TRF4_intervencaoMP + "'"
							: 'NULL'
					}
					,  ${
						precaData.TRF4_maior60Anos != undefined
							? "'" + precaData.TRF4_maior60Anos + "'"
							: 'NULL'
					}
					)`
			);
		} catch (error) {
			console.error(error);
			console.debug(
				'Dump data:',
				`insert into ${
					precaData.TRF4_valorCausa == 0 || precaData.TRF4_valorCausa >= 63000
						? 'viseo.trf4_precatorios'
						: 'viseo.trf4_precatorios_rpv'
				} (
				 TRF4_numPrecatorio,
				 TRF4_numProcessoOriginario,
				 TRF4_dataAutuacao,
				 TRF4_situacao,
				 TRF4_tipoCompetencia,
				 TRF4_tipoCausa,
				 TRF4_cedente,
				 TRF4_requerido,
				 TRF4_juizoOrigem,
				 TRF4_nomeAdvogado,
				 TRF4_dataUltimaAtualizacao,
				 TRF4_textoUltimaAtualizacao,
				 TRF4_anoProposta,
				 TRF4_tutela,
				 TRF4_justicaGratuita,
				 TRF4_valorCausa,
				 TRF4_intervencaoMP,
				 TRF4_maior60Anos)
			values (
					'${precaData.TRF4_numPrecatorio}'
				, '${precaData.TRF4_numProcessoOriginario}'
				, '${precaData.TRF4_dataAutuacao}'
				, '${precaData.TRF4_situacao}'
				, '${precaData.TRF4_tipoCompetencia}'
				, '${precaData.TRF4_tipoCausa}'
				, '${precaData.TRF4_cedente}'
				, '${precaData.TRF4_requerido}'
				, '${precaData.TRF4_juizoOrigem}'
				, '${precaData.TRF4_nomeAdvogado}'
				, '${precaData.TRF4_dataUltimaAtualizacao}'
				, '${precaData.TRF4_textoUltimaAtualizacao}'
				,  ${precaData.TRF4_anoProposta}
				,  ${
					precaData.TRF4_tutela != undefined
						? "'" + precaData.TRF4_tutela + "'"
						: 'NULL'
				}
				,  ${
					precaData.TRF4_justicaGratuita != undefined
						? "'" + precaData.TRF4_justicaGratuita + "'"
						: 'NULL'
				}
				,  ${
					precaData.TRF4_valorCausa != undefined
						? "'" + precaData.TRF4_valorCausa + "'"
						: 'NULL'
				}
				,  ${
					precaData.TRF4_intervencaoMP != undefined
						? "'" + precaData.TRF4_intervencaoMP + "'"
						: 'NULL'
				}
				,  ${
					precaData.TRF4_maior60Anos != undefined
						? "'" + precaData.TRF4_maior60Anos + "'"
						: 'NULL'
				}
				)`
			);
			process.exit(1);
		}
		console.timeEnd(`${numPrecatorioGerado} - insert no banco`);
		console.timeEnd(`${numPrecatorioGerado} - tempo total`);
	}

	browser.close();
	console.timeEnd(`${QTD_PROCESSOS_TRF4} precatórios - tempo total`);
	// process.exit(0);
}

/**
 *
 * Trata o texto da pagina para extrair informações
 * @function trataHTMLLixo
 *
 * @typedef {object} dados
 * @property {string} precatorio  - versão em texto da pagina do precatório
 * @property {string} originario = versão em texto da pagina do processo originario se existir
 *
 * @typedef {object} ObjSaida
 * @property {string} TRF4_numPrecatorio - número do precatório pesquisado.
 * @property {string} TRF4_numProcessoOriginario - número do processo originario (permite pesquisar detalhes do processo).
 * @property {date} TRF4_dataAutuacao - data em que o processo virou precatório. Transformar localtime em UTC no formato YYYY-MM-DD hh:mm:ss.
 * @property {string} TRF4_tipoCompetencia - tipo de documento > Requisição de Pagamento.
 * @property {string}	TRF4_tipoCausa - motivador da geração do processo.
 * @property {string|array}	TRF4_cedente - nomes de todos registrados como cedente (também chamado de requerente) do precatório.
 * @property {string}	TRF4_requerido - contra quem o processo foi movido.
 * @property {string}	TRF4_juizoOrigem - onde foi julgado o processo.
 * @property {string|array}	TRF4_nomeAdvogado - nomes de todos apontados como advogados no processo.
 *
 * @param {dados} data - O texto encontrado dentro da DIV #divConteudo no site do TRF4.
 *
 * @returns {ObjSaida}
 */
function trataHTMLLixo(dados) {
	// console.debug(dados);

	const TEM_ORIGINARIO = `Consulta Processual Unificada - Resultado da Pesquisa`;
	const ANO = moment().month() < 6 ? moment().year() + 1 : moment().year() + 2;

	const PAGO = `BAIXADO - REQUISIÇÃO PAGAMENTO`;

	const regexp_numPrecatorio =
		/((?<=Precatório Nº )(\d{7}-\d{2}.\d{4}.\d{1}.\d{2}.\d{4}){1})/gi;

	const regexp_numProcessoOriginario =
		/((?<=Originário:\s*N. )(\d{7}\d{2}\d{4}\d{1}\d{2}\d{4})){1}/gi;

	const regexp_dataAutuacao =
		/((?<=Data de autuação: )(\d{2}\/{1}\d{2}\/{1}\d{4} \d{2}:\d{2}(:\d{2})?)){1}/gi;

	const regexp_tipoCompetencia = /((?<=Competência: ).*(?=Assuntos:))/gi;

	const regexp_tipoCausa = /(?<=Assuntos:\s+\d{1}.\s{1})((\S+\s{1})+)/gi;

	const regexp_situacao = /(?<=Situação:\s+)(\S+\s?-?\s?\S+\s?\S+)(?=Valor)/gi;

	const regexp_cedente = /((?<=REQUERENTE: )((\S\s?(\s?-\s)?)*))/gi;

	const regexp_cedenteOriginario = /((?<=EXEQUENTE: )((\S\s?(\s?-\s)?)*))/gi;

	const regexp_requerido = /((?<=REQUERIDO: )((\S{1}\s?(\s?-\s)?)*))/gi;

	const regexp_juizoOrigem = /((?<=DEPRECANTE: )((\S{1}\s{0,1})*))/gi;

	const regexp_nomeAdvogado =
		/((?<=Nome: )((\S{1}\s{0,1})*)(?=\(Advogado do REQUERENTE\)))/gi;

	const regexp_nomeAdvogadoOriginario =
		/(?<=Nome:\s)((\S{1}\s{0,1})*)(?=\s\(Advogado\sdo\sEXEQUENTE\))/gi;

	const regexp_dataUltimaAtualizacao =
		/(?:(\d{2}\/\d{2}\/\d{4}\s\d{2}:\d{2})(?!:\d{2}))/gi;

	const regexp_textoUltimaAtualizacao =
		/((?<=\s{2}-\s{1}\d{1,2}\.\s{1})([a-zA-Z\u00C0-\u024F\u1E00-\u1EFF]*[\s{1}\-\,\d{4}>]?(?!\d{2}\/))*)/gi;

	const regexp_anoProposta = /(?<=proposta\sorçamentária\sde\s)(.{4})/gi;

	const regexp_tutela = /(?<=Tutela:\s)(\S*\s{0,1}\S*)(?=Juiz:)/gi;

	const regexp_justicaGratuita =
		/(?<=Justiça gratuita:\s)(\S*\s{0,1}\S*)(?=Valor da causa:)/gi;

	const regexp_valorCausa =
		/(?<=Valor da causa:\s)([0-9\.]+)(?=Intervenção MP:)/gi;

	const regexp_intervencaoMP =
		/(?<=Intervenção\sMP:\s)(.{3})(?=Maior de 60 anos:)/gi;

	const regexp_maior60Anos =
		/(?<=Maior\sde\s60\sanos:\s)(.{3})(?=Competência:)/gi;

	const FORMATO_DATA_TRF4_COM_SEGUNDO = 'DD/MM/YYYY HH:mm:ss';
	const FORMATO_DATA_TRF4_SEM_SEGUNDO = 'DD/MM/YYYY HH:mm';
	const FORMATO_DATA_HORA_BANCO = 'YYYY-MM-DD HH:mm:ss';
	const FORMATO_DATA_BANCO = 'YYYY-MM-DD';

	const MSG_INFORMACAO_FALTANTE = `Não informado`;

	let ObjSaida = {
		TRF4_originarioEletronico: 1,
	};

	if (!dados.originario.startsWith(TEM_ORIGINARIO)) {
		dados.originario = undefined;
		ObjSaida.TRF4_originarioEletronico = 0;
	}

	// console.log(
	// 	filtraCedentes(
	// 		dados.originario?.match(regexp_cedenteOriginario) ||
	// 			dados.precatorio.match(regexp_cedente),
	// 		dados.originario?.match(regexp_nomeAdvogadoOriginario) ||
	// 			dados.precatorio.match(regexp_nomeAdvogado)
	// 	)
	// );

	ObjSaida.TRF4_numPrecatorio = dados.precatorio
		.match(regexp_numPrecatorio)[0]
		.trim();
	ObjSaida.TRF4_numProcessoOriginario = dados.precatorio
		.match(regexp_numProcessoOriginario)[0]
		.trim();
	ObjSaida.TRF4_dataAutuacao = moment(
		dados.precatorio.match(regexp_dataAutuacao),
		FORMATO_DATA_TRF4_COM_SEGUNDO
	)
		.utc()
		.format(FORMATO_DATA_HORA_BANCO);
	ObjSaida.TRF4_situacao = dados.precatorio.match(regexp_situacao)[0];
	ObjSaida.TRF4_tipoCompetencia = dados.precatorio
		.match(regexp_tipoCompetencia)[0]
		.trim();
	ObjSaida.TRF4_tipoCausa = dados.precatorio.match(regexp_tipoCausa)[0].trim();
	ObjSaida.TRF4_cedente = filtraCedentes(
		dados.originario?.match(regexp_cedenteOriginario) ||
			dados.originario?.match(regexp_cedente) ||
			dados.precatorio.match(regexp_cedente),
		dados.originario?.match(regexp_nomeAdvogadoOriginario) ||
			dados.precatorio.match(regexp_nomeAdvogado)
	);
	ObjSaida.TRF4_requerido = dados.precatorio.match(regexp_requerido)[0].trim();
	ObjSaida.TRF4_juizoOrigem = dados.precatorio
		.match(regexp_juizoOrigem)[0]
		.trim();
	ObjSaida.TRF4_nomeAdvogado = filtraCedentes(
		dados.originario?.match(regexp_nomeAdvogadoOriginario) ||
			dados.precatorio.match(regexp_nomeAdvogado)
	);
	ObjSaida.TRF4_dataUltimaAtualizacao = moment(
		dados.precatorio.match(regexp_dataUltimaAtualizacao)[0].trim(),
		FORMATO_DATA_TRF4_SEM_SEGUNDO
	)
		.utc()
		.format(FORMATO_DATA_HORA_BANCO);
	ObjSaida.TRF4_textoUltimaAtualizacao = dados.precatorio
		.match(regexp_textoUltimaAtualizacao)[0]
		.trim();

	if (dados.precatorio.match(regexp_situacao)[0] === PAGO) {
		ObjSaida.TRF4_anoProposta = null;
	} else {
		ObjSaida.TRF4_anoProposta = `${moment(
			dados.precatorio.match(regexp_anoProposta) ?? ANO
		)
			.utc()
			.format(FORMATO_DATA_BANCO)}`;
	}

	if (dados.originario != undefined) {
		// console.log(dados.originario);
		try {
			ObjSaida.TRF4_tutela = dados.originario.match(regexp_tutela)[0]?.trim();
		} catch {
			ObjSaida.TRF4_tutela = MSG_INFORMACAO_FALTANTE;
		}
		try {
			ObjSaida.TRF4_justicaGratuita = dados.originario
				.match(regexp_justicaGratuita)[0]
				?.trim();
		} catch {
			ObjSaida.TRF4_justicaGratuita = MSG_INFORMACAO_FALTANTE;
		}
		try {
			ObjSaida.TRF4_valorCausa = dados.originario
				.match(regexp_valorCausa)[0]
				?.trim();
		} catch {
			ObjSaida.TRF4_valorCausa = 0.0;
		}
		try {
			ObjSaida.TRF4_intervencaoMP = dados.originario
				.match(regexp_intervencaoMP)[0]
				?.trim();
		} catch {
			ObjSaida.TRF4_intervencaoMP = MSG_INFORMACAO_FALTANTE;
		}
		try {
			ObjSaida.TRF4_maior60Anos = dados.originario
				.match(regexp_maior60Anos)[0]
				?.trim();
		} catch {
			ObjSaida.TRF4_maior60Anos = MSG_INFORMACAO_FALTANTE;
		}
	}

	// console.log(dados.originario?.match(regexp_cedenteOriginario));

	// console.log(dados.originario?.match(regexp_nomeAdvogadoOriginario));

	// console.log(ObjSaida);

	// process.exit(1);
	return ObjSaida;
}

/**
 * Essa função tem como objetivo filtrar a lista de cedentes, deixando somente pessoa fisica.
 * @function filtraCedentes
 *
 * @param {array} arrCedentes - Array contendo todos os nomes encontrados como cedente.
 * @param {array} [advogado] - Nome do advogado da causa para remover caso ele apareça também nos cedentes.
 *
 * @returns {string}
 *
 */
function filtraCedentes(arrCedentes, arrAdvogados = []) {
	const regexp_worldList = /(ADVO\w*|ASSOCIAD\w*)/gi;
	const advogados = arrAdvogados?.map((el) => el.trim());
	let cedentes = arrCedentes
		?.map((el) => el.trim().replaceAll(`'`, ''))
		.filter(
			(ced) => advogados?.indexOf(ced) === -1 && !ced.match(regexp_worldList)
		);

	if (cedentes?.length >= 5) {
		cedentes.length = 5;
	} else if (cedentes?.length === 0) {
		cedentes = arrCedentes?.map((el) => el.trim().replaceAll(`'`, ''));
	}

	return cedentes?.join(',');
}

async function buscaDados(
	numPrecatorio,
	numCPF,
	txtPalavraGerada,
	hdnRefId,
	browser
) {
	let URL = `${BASE_URL}?${new URLSearchParams({
		...params,
		txtChave: numCPF ? numCPF : '',
		txtPalavraGerada: txtPalavraGerada ? txtPalavraGerada : '',
		hdnRefId: hdnRefId ? hdnRefId : '',
		txtValor: numPrecatorio,
	})}`;

	const page = await browser.newPage();
	page.setDefaultNavigationTimeout(0);

	await page.goto(URL, {
		waitUntil: 'networkidle0',
	});

	try {
		await page.click('#btnAceitoPoliticaPrivacidade');
	} catch (error) {
		// console.log(error);
	}

	let requestID;

	await page.on('dialog', async (diag) => {
		console.debug(diag._message);
		if (diag._message === 'Captcha inválido.') {
			console.log(
				(
					await axios.get(
						`http://2captcha.com/res.php?key=${process.env.TWOCAPTCHA_API_KEY}&action=reportbad&id=${requestID.data.request}`
					)
				).data
			);
			await diag.dismiss();
			throw new Error('Captcha inválido.');
		}
		return diag._message;
	});

	try {
		let imgHandle = await page.$('#imgCaptcha');
		let img = await imgHandle.screenshot({ encoding: 'base64' });

		requestID = await axios.post(process.env.TWOCAPTCHA_IN_URL, {
			...captchaOptions,
			body: img,
		});

		// await page.waitForSelector('[name=g-recaptcha-response]', { timeout: 500 });

		// let requestID = await axios.post(process.env.TWOCAPTCHA_IN_URL, {
		// 	...captchaOptions,
		// 	pageurl: page.url(),
		// });

		console.log(requestID.data);

		await timeout(15 * 1000);

		let response,
			count = 0;

		do {
			await timeout(5 * 1000);

			response = await axios.get(
				`${process.env.TWOCAPTCHA_RES_URL}?${new URLSearchParams({
					...respOptions,
					id: requestID.data.request,
				})}`
			);

			// console.log(
			// 	`${process.env.TWOCAPTCHA_RES_URL}?${new URLSearchParams({
			// 		...respOptions,
			// 		id: requestID.data.request,
			// 	})}`
			// );

			console.log(response.data.status);

			count++;

			if (count >= 10) {
				throw new Error('Falha ao obter captcha');
			}
		} while (response.data.status != 1);

		// await page.evaluate((token) => {
		// 	// document
		// 	// 	.querySelector('[name=g-recaptcha-response]')
		// 	// 	.removeAttribute('style');
		// 	// document
		// 	// 	.querySelector('[name=h-captcha-response]')
		// 	// 	.removeAttribute('style');

		// 	// document.querySelector('[name=g-recaptcha-response]').innerText = token;
		// 	// document.querySelector('[name=h-captcha-response]').innerText = token;
		// 	document.querySelector('#txtInfraCaptcha').value = token;
		// }, response.data.request);

		await page.type('#txtInfraCaptcha', response.data.request);

		// await timeout(30 * 1000);

		await Promise.all([
			page.waitForNavigation({ waitUntil: 'networkidle0' }),
			page.click('#frmValidaPesquisa > input.botao', { delay: 100 }),
		]);
	} catch (error) {
		// console.log(error);
		// process.exit(1);
	}

	let objPaginas = { precatorio: undefined, originario: undefined };

	let linkOriginario = await page.evaluate(() => {
		return document.querySelector('#divConteudo > a:nth-child(6)').href;
	});

	const garbageHTML = await page.content();

	const $1 = cheerio.load(garbageHTML, {
		decodeEntities: false,
	});

	objPaginas.precatorio = $1('#divConteudo').text();

	await page.on('dialog', async (msg) => {
		await msg.dismiss();
	});

	await page.goto(`${linkOriginario}&todaspartes=S`, {
		waitUntil: 'networkidle0',
	});

	const garbageHTMLOriginario = await page.content();

	const $2 = cheerio.load(garbageHTMLOriginario, {
		decodeEntities: false,
	});

	objPaginas.originario = $2('#divConteudo').text();

	await page.close();

	return objPaginas;
}

while (true) {
	await autobotTRF4(mysqlPool);
}

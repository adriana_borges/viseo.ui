import 'dotenv/config.js';
import puppeteer from 'puppeteer';
import axios from 'axios';
import moment from 'moment';
import Sequelize from 'sequelize';

const sequelize = new Sequelize(
	process.env.MYSQL_DB,
	process.env.MYSQL_USER,
	process.env.MYSQL_PASS,
	{
		host: process.env.MYSQL_CLUSTER_PROD,
		dialect: 'mysql',
	}
);

const TRF3Details = {
	sitekey: '6LcMoT0aAAAAAL36YkPdesuSXnXQgmVSc-cmnvU6',
	pageurl: 'https://web3.trf3.jus.br/consultas/Internet/ConsultaReqPag',
};

const captchaAPI = 'ff6bfa1e675a0d74cc23a162f733b2d7';
const VALOR_MINIMO_PRC = 63_000;

const pageSelectors = {
	input: {
		cpf: '#CpfCnpj',
		protocolo: '#NumeroProtocolo',
	},
	button: {
		pesquisar: '#pesquisar',
	},
};

const MSG_SEM_PRECATORIOS_NO_CPF =
	'Não há ofícios que atendem aos critérios de pesquisa.';

const anoPrecatorio = process.env.ANO;

let numPrecatorio = function* (ano) {
	//Generator para número de precatorios do TRF3 ex: YYYYxxxxxxx
	let start = process.env.START || '00000';
	let i = Number(`${ano}${start}${''.padStart(2, '0')}`, 10);
	while (true) {
		yield i++;
	}
};

mainmain();

async function mainmain() {
	let counter = process.env.QTD || 99;
	let errcounter = 0;
	const browser = await puppeteer.launch({
		headless: true,
		// executablePath: '/usr/bin/microsoft-edge-dev',
		slowMo: 50,
		defaultViewport: null,
	});

	do {
		try {
			await main(browser, counter);
			counter--;
			if (errcounter > 0) {
				errcounter--;
			}
		} catch (e) {
			errcounter++;
			console.error('Deu erro, tentando de novo...', e);
		}
	} while (counter >= 0 && errcounter <= 2);
	browser.close();
}

async function main(browser, counter) {
	if (counter <= 0) {
		return;
	}
	console.log({
		level: 'info',
		message: `Saldo do 2Captcha: ${
			(
				await axios.get(
					`https://2captcha.com/res.php?key=${captchaAPI}&action=getbalance&json=1`
				)
			).data.request
		}`,
	});

	let strikes = 0;

	let excludeList = await sequelize.query(
		`select distinct a.numero from (SELECT distinct numero FROM viseo.requisicoes where tribunal = 'TRF3' and left(numero,4)=${anoPrecatorio}
			union all
			select distinct TRF3_numeroProtocolo from viseo.trf3_precatorios where left(TRF3_numeroProtocolo,4) = ${anoPrecatorio}
			union all 
			select distinct TRF3_numeroProtocolo from viseo.trf3_precatorios_rpv where left(TRF3_numeroProtocolo,4) = ${anoPrecatorio}) as a order by a.numero
		`,
		{ raw: true, type: sequelize.QueryTypes.SELECT }
	);

	excludeList = excludeList.map((el) => el.numero);

	const genPrecatorios = numPrecatorio(anoPrecatorio);

	let nextPrecatorio;

	while (counter > 0) {
		do {
			nextPrecatorio = genPrecatorios.next().value.toString();
		} while (excludeList.indexOf(nextPrecatorio) != -1);

		const page = await browser.newPage();

		page.setUserAgent(
			'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4464.0 Safari/537.36 Edg/91.0.852.0'
			//'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1'
		); //Muda o User Agent para poder usar o headless

		try {
			let temPreca = await buscaPrecatoriosTRF3(nextPrecatorio, page);

			if (temPreca?.error === undefined) {
				await sequelize.query(
					`
						INSERT INTO ${
							temPreca?.TRF3_procedimento === 'RPV' ||
							temPreca.TRF3_valor_solicitado < VALOR_MINIMO_PRC ||
							temPreca.TRF3_valor_inscrito_proposta < VALOR_MINIMO_PRC
								? 'viseo.trf3_precatorios_rpv'
								: 'viseo.trf3_precatorios'
						}
							(TRF3_numeroProtocolo,
							TRF3_procedimento,
							TRF3_CNJ,
							TRF3_data_protocolo,
							TRF3_situacao,
							TRF3_oficio_requisitorio,
							TRF3_juizo_origem,
							TRF3_processo_originario,
							TRF3_requerido,
							TRF3_requerente,
							TRF3_advogado,
							TRF3_ano_proposta,
							TRF3_data_conta_liquidacao,
							TRF3_valor_solicitado,
							TRF3_valor_inscrito_proposta,
							TRF3_requisicao_bloqueada,
							TRF3_situacao_requisicao,
							TRF3_natureza,
							TRF3_banco)
							VALUES
							('${temPreca.TRF3_numeroProtocolo}',
							'${temPreca.TRF3_procedimento}',
							'${temPreca.TRF3_CNJ}',
							'${temPreca.TRF3_data_protocolo}',
							'${temPreca.TRF3_situacao}',
							'${temPreca.TRF3_oficio_requisitorio}',
							'${temPreca.TRF3_juizo_origem}',
							'${temPreca.TRF3_processo_originario}',
							'${temPreca.TRF3_requerido}',
							'${temPreca.TRF3_requerente}',
							'${temPreca.TRF3_advogado}',
							'${temPreca.TRF3_ano_proposta}',
							'${temPreca.TRF3_data_conta_liquidacao}',
							${temPreca.TRF3_valor_solicitado},
							${
								temPreca.TRF3_valor_inscrito_proposta == undefined
									? 0
									: temPreca.TRF3_valor_inscrito_proposta
							},
							'${temPreca.TRF3_requisicao_bloqueada}',
							'${temPreca.TRF3_situacao_requisicao}',
							'${temPreca.TRF3_natureza}',
							'${temPreca.TRF3_banco}')
								`,
					{
						raw: true,
						replacements: [],
						type: sequelize.QueryTypes.INSERT,
					}
				);

				console.log({
					level: 'info',
					message: `Inserido o precatório ${nextPrecatorio}`,
				});
				counter--;
			}
		} catch (e) {
			console.log(e, temPreca);
			process.exit(1);
		}
	}

	browser.close();
}

async function getCPF() {
	return await sequelize.query(
		`SELECT documento, 
            id, 
            numero 
      FROM viseo.requisicoes 
      where documento <> '00000000000' 
        and id_posicionamento is null 
				and tribunal = 'TRF3'
				and documento not in (select documento from viseo.bot_validacao)`,
		{
			raw: true,
			replacements: [],
			type: sequelize.QueryTypes.SELECT,
		}
	);
}

async function buscaPrecatoriosTRF3(client, page) {
	let temPrecatorio = 0;
	console.log(client);

	try {
		await page.goto(TRF3Details.pageurl);
		let requestId;
		do {
			requestId = (
				await axios.post(
					`https://2captcha.com/in.php?key=${captchaAPI}&method=userrecaptcha&googlekey=${TRF3Details.sitekey}&pageurl=${TRF3Details.pageurl}&json=1`
				)
			).data.request;

			await timeout(5000);
		} while (requestId === 'ERROR_NO_SLOT_AVAILABLE');

		console.log({
			level: 'debug',
			message: `RequestID: ${requestId}`,
		});

		await page.evaluate(
			`document.getElementById("pesquisar").removeAttribute("disabled")`
		);

		await page.type(pageSelectors.input.protocolo, client.toString());

		await timeout(15000);

		let response;
		let tries = 0;
		do {
			await timeout(5000);
			response = await axios.get(
				`https://2captcha.com/res.php?key=${captchaAPI}&action=get&id=${requestId}&json=1`
			);

			console.log({
				level: 'debug',
				message: `Tentativa ${tries + 1}: ${JSON.stringify(response.data)}`,
			});

			tries++;
			if (tries >= 25) {
				break;
			}
		} while (response.data.status != 1);

		console.log({
			level: 'debug',
			message: `Captcha terminado em ${tries} tentativas.`,
		});
		if (tries < 20) {
			await page.evaluate(
				`document.getElementById("g-recaptcha-response").innerHTML="${response.data.request}";`
			);

			await page.click(pageSelectors.button.pesquisar);

			await timeout(300);

			try {
				await page.waitForSelector(
					'#grid > tbody > tr:nth-child(1) > td.celula',
					{
						waitUntil: 'load',
						timeout: 30000,
					}
				);

				const tb = await page.evaluate(() => {
					let JSObject = {
						TRF3_numeroProtocolo: undefined,
						TRF3_procedimento: undefined,
						TRF3_CNJ: undefined,
						TRF3_data_protocolo: undefined,
						TRF3_situacao: undefined,
						TRF3_oficio_requisitorio: undefined,
						TRF3_juizo_origem: undefined,
						TRF3_processo_originario: undefined,
						TRF3_requerido: undefined,
						TRF3_requerente: undefined,
						TRF3_advogado: 'Não localizado',
						TRF3_ano_proposta: undefined,
						TRF3_data_conta_liquidacao: undefined,
						TRF3_valor_solicitado: undefined,
						TRF3_valor_inscrito_proposta: undefined,
						TRF3_requisicao_bloqueada: undefined,
						TRF3_situacao_requisicao: undefined,
						TRF3_banco: 'Não informado',
						TRF3_natureza: undefined,
						error: undefined,
					};

					const dataTable = document.querySelectorAll('#grid tr');

					let title;
					let content;

					dataTable.forEach((el) => {
						let cel = el.querySelectorAll('td');
						title = (cel[0].innerText || 'requerentes')
							.toLowerCase()
							.replace(/\s/g, '');
						content = cel[1].innerText;

						switch (title) {
							case 'procedimento':
								if (JSObject.TRF3_procedimento == undefined) {
									JSObject.TRF3_procedimento = content;
								}
								break;
							case 'número':
								if (JSObject.TRF3_numeroProtocolo == undefined) {
									JSObject.TRF3_numeroProtocolo = content;
								}
								break;
							case 'número-cnj':
								if (JSObject.TRF3_CNJ == undefined) {
									JSObject.TRF3_CNJ = content;
								}
								break;
							case 'dataprococolotrf':
								if (JSObject.TRF3_data_protocolo == undefined) {
									JSObject.TRF3_data_protocolo = content;
								}
								break;
							case 'situaçãodoprotocolo':
								if (JSObject.TRF3_situacao == undefined) {
									JSObject.TRF3_situacao = content;
								}
								break;
							case 'ofíciorequisitório':
								if (JSObject.TRF3_oficio_requisitorio == undefined) {
									JSObject.TRF3_oficio_requisitorio = content;
								}
								break;
							case 'juízodeorigem':
								if (JSObject.TRF3_juizo_origem == undefined) {
									JSObject.TRF3_juizo_origem = content;
								}
								break;
							case 'processosoriginários':
								if (JSObject.TRF3_processo_originario == undefined) {
									JSObject.TRF3_processo_originario = content
										.split('\n')[0]
										.replace(/[\<\>br\s]/g, '');
								}
								break;
							case 'requerido':
								if (JSObject.TRF3_requerido == undefined) {
									JSObject.TRF3_requerido = content.replace(/[\']/gi, '');
								}
								break;
							case 'requerentes':
								if (
									JSObject.TRF3_requerente == undefined &&
									content.length > 0
								) {
									JSObject.TRF3_requerente = content.replace(/[\']/gi, '');
								}
								break;
							case 'advogado':
								JSObject.TRF3_advogado = content
									.substring(0, 100)
									.replace(/[\']/gi, '');
								break;
							case 'anodaproposta':
								if (JSObject.TRF3_ano_proposta == undefined) {
									JSObject.TRF3_ano_proposta = content;
								}
								break;
							case 'mês/anodaproposta':
								if (JSObject.TRF3_ano_proposta == undefined) {
									JSObject.TRF3_ano_proposta = content;
								}
								break;
							case 'datacontadeliquidação':
								if (JSObject.TRF3_data_conta_liquidacao == undefined) {
									JSObject.TRF3_data_conta_liquidacao = content;
								}
								break;
							case 'banco':
								JSObject.TRF3_banco = content;
								break;
							case 'valorsolicitado':
								if (JSObject.TRF3_valor_solicitado == undefined) {
									JSObject.TRF3_valor_solicitado = content.replace(
										/[\sR$.]/g,
										''
									);
								}
								break;
							case 'valorinscritonaproposta':
								if (JSObject.TRF3_valor_inscrito_proposta == undefined) {
									JSObject.TRF3_valor_inscrito_proposta = content.replace(
										/[\sR$.]/g,
										''
									);
								}
								break;
							case 'requisiçãobloqueada':
								if (JSObject.TRF3_requisicao_bloqueada == undefined) {
									JSObject.TRF3_requisicao_bloqueada = content;
								}
								break;
							case 'situaçãodarequisição':
								if (JSObject.TRF3_situacao_requisicao == undefined) {
									JSObject.TRF3_situacao_requisicao = content;
								}
								break;
							case 'natureza':
								if (JSObject.TRF3_natureza == undefined) {
									JSObject.TRF3_natureza = content;
								}
								break;
						}
					});

					return JSObject;
				});

				const data = await sanitizeData(tb);

				await page.close();
				return data;
			} catch (error) {
				console.log('Error while loading the data:', error);

				let lb = await page.evaluate(() => {
					return document.querySelector('#conteudoPrincipal > label');
				});

				if ((lb = 'Não há ofícios que atendem aos critérios de pesquisa.')) {
					const data = {
						TRF3_numeroProtocolo: client.toString(),
						TRF3_procedimento: '',
						TRF3_CNJ: '',
						TRF3_data_protocolo: '1970-01-01 00:00:00',
						TRF3_situacao: '',
						TRF3_oficio_requisitorio: '',
						TRF3_juizo_origem: '',
						TRF3_processo_originario: '',
						TRF3_requerido: '',
						TRF3_requerente: '',
						TRF3_advogado: '',
						TRF3_ano_proposta: '1970-01-01 00:00:00',
						TRF3_data_conta_liquidacao: '1970-01-01 00:00:00',
						TRF3_valor_solicitado: 0,
						TRF3_valor_inscrito_proposta: 0,
						TRF3_requisicao_bloqueada: '',
						TRF3_situacao_requisicao: '',
						TRF3_banco: '',
						TRF3_natureza: '',
						error: undefined,
					};
				}
			}

			return data;
		} else {
			const data = { error: 'error' };
			console.log({
				level: 'warn',
				message: 'Erro ao obter dados... Reiniciando processo.',
			});
			await page.close();

			return data;
		}
	} catch (error) {}
}

function sanitizeData(dirty) {
	const mysqlDateTimeFormat = 'YYYY-MM-DD HH:mm:ss';
	const mysqlDateFormat = 'YYYY-MM-DD';
	let clean = dirty;

	clean.TRF3_data_protocolo = moment(
		dirty.TRF3_data_protocolo,
		'DD/MM/YYYY HH:mm:ss'
	)
		.utc()
		.format(mysqlDateTimeFormat);

	clean.TRF3_data_conta_liquidacao = moment(
		dirty.TRF3_data_conta_liquidacao,
		'DD/MM/YYYY'
	)
		.utc()
		.format(mysqlDateFormat);

	if (dirty.TRF3_ano_proposta.length == 4) {
		clean.TRF3_ano_proposta = moment([dirty.TRF3_ano_proposta])
			.utc()
			.format(mysqlDateFormat);
	} else {
		let mesano = dirty.TRF3_ano_proposta.split('/');

		clean.TRF3_ano_proposta = moment([mesano[1], parseInt(mesano[0]) - 1])
			.utc()
			.format(mysqlDateFormat);
	}

	clean.TRF3_valor_solicitado = dirty.TRF3_valor_solicitado.replace(',', '.');

	try {
		clean.TRF3_valor_inscrito_proposta =
			dirty.TRF3_valor_inscrito_proposta.replace(',', '.');
	} catch {}

	return clean;
}

const timeout = (millis) =>
	new Promise((resolve) => setTimeout(resolve, millis));

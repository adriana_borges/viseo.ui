import {} from 'dotenv/config.js';
import puppeteer from 'puppeteer';
import axios from 'axios';
import moment from 'moment';
import mysqlPool from '../db/mysql/prod.js';
import wget from 'node-wget-promise';
import { PdfDataExtractor } from 'pdfdataextract';
import { readFileSync } from 'fs';
import aws from 'aws-sdk';

aws.config.update({
	accessKeyId: process.env.AWS_ACCESS_KEY_ID,
	secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
	region: process.env.AWS_REGION,
});

const options = {
	Bucket: process.env.BUCKET,
	region: process.env.REGION,
	signatureVersion: 'v4',
	ACL: 'public-read',
};

const s3 = new aws.S3(options);

const BASE_URL = `https://www2.trf4.jus.br/trf4/controlador.php`;

const MSG_PRECA_NAO_ENCONTRADO =
	'Precatório não encontrado: verifique se os dados informados foram digitados corretamente.';

const RETORNO_CPF_INVALIDO = 'CPF INVALIDO';

const PRECATORIO_JA_PAGO = 'DEMONSTRATIVO DE TRANSFERÊNCIA1';

const USER_AGENT =
	'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36 Edg/92.0.902.55';

const timeout = (millis) =>
	new Promise((resolve) => setTimeout(resolve, millis));

const captchaOptions = {
	key: process.env.TWOCAPTCHA_API_KEY,
	numeric: 2, // * 2 - captcha contains only letters
	sitekey: process.env.TRF4_HCAPTCHA_SITE_KEY,
	method: 'hcaptcha',
	userAgent: USER_AGENT,
	header_acao: 1,
	json: 1,
};

const respOptions = {
	key: process.env.TWOCAPTCHA_API_KEY,
	action: 'get',
	json: 1,
};

let params = {
	acao: 'pagina_precatorios',
	hdnRefId: undefined, //! Suspeito que seja um hash do cookie que permite fazer consultas subsequentes sem digitar um novo captcha
	selForma: 'NC', //* 'NU' para consultas simples e 'NC' para consultas com CPF
	selOrigem: 'TRF', //* Origem dos dados
	todasfases: 'S', //* Mostra todas as fases do processo
	todaspartes: 'S', //* Mostra o nome de todos os advogados no processo
	txtChave: undefined, //* Chave do processo
	txtPalavraGerada: undefined, //* Letras do captcha que aparecem na consulta
	txtValor: undefined, //* Numero do precatório
	paginaSubmeteuPesquisa: 'letras',
	numPagina: '1',
};

// autobotTRF4(mysqlPool);

export default async function autobotTRF4(mysqlPool) {
	const [rows] = await mysqlPool.query(
		`select TRF4_numPrecatorio, bot_candidatoCPF from trf4_precatorios where bot_candidatoCPF is not null and bot_linkPDF is null
		`
	);

	const QTD_PROCESSOS_TRF4 = rows.length;

	console.time(`${QTD_PROCESSOS_TRF4} precatórios - tempo total`);

	const browser = await puppeteer.launch({
		headless: true,
		// executablePath: '/usr/bin/microsoft-edge-dev',
		slowMo: 30,
		defaultViewport: null,
	});

	for (let i = 0; i < QTD_PROCESSOS_TRF4; i++) {
		let doAgain;
		let tries = 0;
		let numPrecatorioGerado;
		let dados;
		do {
			numPrecatorioGerado = rows[i];
			// console.log(numPrecatorioGerado);
			console.time(`${numPrecatorioGerado.TRF4_numPrecatorio} - tempo total`);
			console.time(
				`${numPrecatorioGerado.TRF4_numPrecatorio} - busca de dados`
			);
			dados = await buscaDados(
				numPrecatorioGerado.TRF4_numPrecatorio,
				numPrecatorioGerado.bot_candidatoCPF,
				params.txtPalavraGerada,
				params.hdnRefId,
				browser
			);

			console.log('dados:', dados);

			if (dados === 'ERROR' || dados === undefined) {
				doAgain = true;
				params.hdnRefId = undefined;
				params.txtPalavraGerada = undefined;
				tries++;
			} else {
				doAgain = false;
				tries = 0;
			}
		} while (doAgain && tries < 5);

		console.timeEnd(
			`${numPrecatorioGerado.TRF4_numPrecatorio} - busca de dados`
		);

		console.time(`${numPrecatorioGerado.TRF4_numPrecatorio} - insert no banco`);
		try {
			if (dados === RETORNO_CPF_INVALIDO) {
				await mysqlPool.query(
					`	update trf4_precatorios
					 	set bot_candidatoCPF = NULL
						 	,bot_CPFValidado = 0
							,bot_linkPDF = NULL
							,bot_dataUltimaAtualizacao = current_timestamp()
						where TRF4_numPrecatorio = '${numPrecatorioGerado.TRF4_numPrecatorio}'`
				);

				await mysqlPool.query(
					`
							INSERT INTO bot_validacao VALUES ('${numPrecatorioGerado.bot_candidatoCPF}',0)
					`
				);
			} else {
				await mysqlPool.query(
					`update trf4_precatorios
					set bot_CPFValidado = 1
					,bot_linkPDF = '${dados.link}'
					,bot_tipoPDF = '${dados.tipo}'
					,TRF4_mesesExecAnterior = '${dados.MESES_EXEC_ANTERIOR}'
					,TRF4_OAB = '${dados.OAB_ADVOGADO}'
					,TRF4_valorRequisicao = ${dados.VALOR_REQUISICAO}
					,TRF4_honorarios = ${
						isNaN(dados.HONORARIOS)
							? dados.VALOR_REQUISICAO * 0.3
							: dados.HONORARIOS
					}
					,bot_dataUltimaAtualizacao = current_timestamp()
					where TRF4_numPrecatorio = '${numPrecatorioGerado.TRF4_numPrecatorio}'`
				);

				await mysqlPool.query(
					`
							INSERT INTO bot_validacao VALUES ('${numPrecatorioGerado.bot_candidatoCPF}',1)
					`
				);
			}
		} catch (error) {
			console.log('Dump data:', dados);
			console.error(error);
		}
		console.timeEnd(
			`${numPrecatorioGerado.TRF4_numPrecatorio} - insert no banco`
		);
		console.timeEnd(`${numPrecatorioGerado.TRF4_numPrecatorio} - tempo total`);
	}

	browser.close();
	console.timeEnd(`${QTD_PROCESSOS_TRF4} precatórios - tempo total`);
}

async function buscaDados(
	numPrecatorio,
	numCPF,
	txtPalavraGerada,
	hdnRefId,
	browser
) {
	let URL = `${BASE_URL}?${new URLSearchParams({
		...params,
		txtPalavraGerada: txtPalavraGerada ? txtPalavraGerada : '',
		hdnRefId: hdnRefId ? hdnRefId : '',
		txtValor: numPrecatorio.replace(/([\.-])/g, ''),
	})}`;

	let popup = false;

	if (!txtPalavraGerada || !hdnRefId) {
		const page = await browser.newPage();

		page.setDefaultNavigationTimeout(0);

		let diag = await page.on('dialog', async (diag) => {
			console.debug(diag._message);
			if (diag._message === MSG_PRECA_NAO_ENCONTRADO) {
				console.log('Diag correto.');
				await diag.dismiss();
			}
			return diag._message;
		});

		try {
			await page.goto(URL, {
				waitUntil: 'networkidle0',
			});

			await page.type('#txtCpfCnpj', numCPF);
			await page.type('#txtNumeroProcesso', numPrecatorio);

			await page.click('#sbmConsultar');

			try {
				await page.waitForSelector('[name=g-recaptcha-response]', {
					timeout: 1000,
				});

				let requestID = await axios.post(process.env.TWOCAPTCHA_IN_URL, {
					...captchaOptions,
					pageurl: page.url(),
				});

				console.log(requestID.data);

				await timeout(15 * 1000);
				let response;

				do {
					await timeout(5 * 1000);
					response = await axios.get(
						`${process.env.TWOCAPTCHA_RES_URL}?${new URLSearchParams({
							...respOptions,
							id: requestID.data.request,
						})}`
					);
					console.log(response.data.status);
				} while (response.data.status != 1);

				await page.evaluate((token) => {
					document
						.querySelector('[name=g-recaptcha-response]')
						.removeAttribute('style');
					document
						.querySelector('[name=h-captcha-response]')
						.removeAttribute('style');

					document.querySelector('[name=g-recaptcha-response]').innerText =
						token;
					document.querySelector('[name=h-captcha-response]').innerText = token;
					document.querySelector('#frmValidaPesquisa > input.botao').click();
				}, response.data.request);
			} catch (error) {}

			await timeout(2 * 1000);

			await page.goto(`${page.url()}&numPagina=1`, {
				waitUntil: 'networkidle0',
			});

			let links = await page.evaluate(() => {
				let links = [];
				document.querySelectorAll('#divConteudo > a').forEach((a) => {
					if (
						a.innerText.match(/REQUISIÇÃO DE PAGAMENTO/, 'gi') ||
						a.innerText.match(/PETIÇÃO INICIAL/, 'gi') ||
						a.innerText.match(/DEMONSTRATIVO DE TRANSFERÊNCIA/, 'gi')
					) {
						links.push({
							link: a.href,
							text: a.innerText,
						});
					}
				});

				return links;
			});

			await page.goto(links[0].link, { waitUntil: 'networkidle0' });

			let trueLink = await page.evaluate(() => {
				return document.querySelector('iframe').src;
			});

			const fileName = `../logs/TRF4/PDFs/${numPrecatorio}_${links[0].text.replace(
				/\s/gi,
				'_'
			)}.pdf`;

			await wget(trueLink, { output: fileName });

			await page.close();

			if (links[0].text === PRECATORIO_JA_PAGO) {
				return {
					MESES_EXEC_ANTERIOR: 0,
					OAB_ADVOGADO: 'Não informado',
					VALOR_REQUISICAO: 0,
					HONORARIOS: 0,
					link: fileName,
					tipo: links[0].text,
				};
			} else {
				let pdfBuffer = readFileSync(fileName);

				// let obj = s3.putObject({
				// 	Bucket: process.env.AWS_BUCKET,
				// 	Key: fileName,
				// 	Body: pdfBuffer,
				// 	acl: 'public-read',
				// });

				let pdf = await PdfDataExtractor.get(pdfBuffer);

				let pdfInfo = await trataPDFLixo(await pdf.getText(10, true));

				return { ...pdfInfo, link: fileName, tipo: links[0].text };
			}
		} catch (err) {
			console.log('ERROR:', err);
			if (popup || diag) {
				return RETORNO_CPF_INVALIDO;
			} else {
				return 'ERROR';
			}
		}
	}
}
let file = '5022521-30.2021.4.04.9388_REQUISIÇÃO_DE_PAGAMENTO1';
let pdfBuffer = readFileSync(
	`C:\Users\luiz.favaro\Documents\projetos\dawnofthenewrobots\logs\TRF4\PDFs\${file}.pdf`
);
let pdf = await PdfDataExtractor.get(pdfBuffer);
let pdfInfo = await trataPDFLixo(await pdf.getText(10, true));

console.log(pdfInfo);

async function trataPDFLixo(pdfLixo) {
	const pdfString = pdfLixo.toString();

	const regexp_MESES_EXEC_ANTERIOR = /(?<=Meses\sExe.\sAnterior)\d*/gim;

	const regexp_OAB_ADVOGADO = /(?<=ADVOGADO:\s.*)\S+(?=\n)/gi;

	const regexp_VALOR_REQUISICAO =
		/(?<=Valor\sTotal\sda\sRequisição:)(\d[,.]?)*/gim;

	const regexp_HONORARIOS_VALOR_PRINCIPAL =
		/(?<=TipoHonorário\n(.*\n)*Valor\s(Principal|Atualizado){1,})(\d[,.]?)*/gim;

	const regexp_HONORARIOS_VALOR_JUROS =
		/(?<=(TipoHonorário\n?(.*\n)*Valor\sJuros){1}(\sAtualiz\.)?)(\d,?\.?)*(?=(Data\s|\n))/gim;

	return {
		MESES_EXEC_ANTERIOR:
			parseInt(pdfString.match(regexp_MESES_EXEC_ANTERIOR)[0], 10) || 0,
		OAB_ADVOGADO: pdfString.match(regexp_OAB_ADVOGADO)[0],
		VALOR_REQUISICAO: pdfString
			.match(regexp_VALOR_REQUISICAO)
			?.map((el) => parseFloat(el.replace('.', '').replace(',', '.')))
			.reduce((acc, val) => acc + val),
		HONORARIOS:
			pdfString
				.match(regexp_HONORARIOS_VALOR_PRINCIPAL)
				?.map((el) => parseFloat(el.replace('.', '').replace(',', '.')))
				?.reduce((acc, val) => acc + val) +
			pdfString
				.match(regexp_HONORARIOS_VALOR_JUROS)
				?.map((el) => parseFloat(el.replace('.', '').replace(',', '.')))
				?.reduce((acc, val) => acc + val),
	};
}

import {} from 'dotenv/config.js';
import moment from 'moment';
import * as robot from '../db/mysql/robots.js';
import * as prod from '../db/mysql/prod.js';

const QUERY_SELECT_NEW_CASES = `
  select a.*, c.*, d.cidade as JO_cidade, d.estado as JO_estado 
  from viseo.trf3_precatorios as a 
  left join viseo.requisicoes as b 
    on b.numero = a.TRF3_numeroProtocolo
  left join viseo.vis_requerentes as c 
    on a.bot_candidatoCPF = c.pes_documento
  inner join viseo.vw_juizo_origem as d 
    on a.TRF3_juizo_origem = d.juizo_origem
  where c.pes_documento is not null
  and b.id is null
`;

const QUERY_INSERT_TELEFONE = `
insert into viseo.telefones (
  id,
  id_requisicoes,
  numero,
  dt_cadastro
) values (
  NULL,
  ?,
  ?,
  ?
)
`;

const QUERY_INSERT_REQUISICOES = `
insert into viseo.requisicoes (
  id,
  documento,
  numero,
  data_protocolo_trf,
  situacao_protocolo,
  oficio_requisitorio,
  juizo_origem,
  processos_originarios,
  requerido,
  requerentes,
  advogado,
  mes_ano_proposta,
  data_conta_liquidacao,
  valor_solicitado,
  valor_inscrito_proposta,
  situacao_proposta,
  banco,
  natureza,
  status,
  cidade,
  estado,
  oab,
  endereco,
  endereco_numero,
  endereco_comp,
  endereco_bairro,
  endereco_cidade,
  endereco_estado,
  endereco_cep,
  tribunal,
  processo
)
values (
  NULL,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  1,
  ?,
  ?,
  'EE999999',
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  'TRF3',
  ?
)
`;

export default async function autobotTRF3(mysqlPoolProd) {
	const [rows] = await mysqlPoolProd.query(QUERY_SELECT_NEW_CASES);

	console.log(`Encontrados ${rows.length} precatorios para inserir.`);
	while (rows.length > 0) {
		let precatorioValidado = rows.pop();

		let [precatorio] = await mysqlPoolProd.query(QUERY_INSERT_REQUISICOES, [
			precatorioValidado.bot_candidatoCPF,
			precatorioValidado.TRF3_numeroProtocolo,
			precatorioValidado.TRF3_data_protocolo,
			precatorioValidado.TRF3_situacao,
			precatorioValidado.TRF3_oficio_requisitorio,
			precatorioValidado.TRF3_juizo_origem,
			precatorioValidado.TRF3_processo_originario,
			precatorioValidado.TRF3_requerido,
			precatorioValidado.TRF3_requerente,
			precatorioValidado.TRF3_advogado,
			precatorioValidado.TRF3_ano_proposta,
			precatorioValidado.TRF3_data_conta_liquidacao,
			precatorioValidado.TRF3_valor_solicitado,
			precatorioValidado.TRF3_valor_inscrito_proposta <
			precatorioValidado.TRF3_valor_solicitado
				? precatorioValidado.TRF3_valor_solicitado
				: precatorioValidado.TRF3_valor_inscrito_proposta,
			precatorioValidado.TRF3_situacao_requisicao,
			precatorioValidado.TRF3_banco,
			precatorioValidado.TRF3_natureza,
			precatorioValidado.JO_cidade,
			precatorioValidado.JO_estado,
			precatorioValidado.end_nome_rua,
			precatorioValidado.end_numero,
			precatorioValidado.end_complemento,
			precatorioValidado.end_bairro,
			precatorioValidado.end_cidade,
			precatorioValidado.end_uf,
			precatorioValidado.end_cep,
			precatorioValidado.TRF3_CNJ,
		]);

		let id = precatorio.insertId;

		console.log(
			`Inserido precatorio ${precatorioValidado.TRF3_numeroProtocolo} com ID ${id}`
		);

		if (precatorioValidado.tel_lista?.length > 0) {
			let telefones = precatorioValidado.tel_lista.split(',');
			console.log(
				`Inserindo ${telefones.length} telefone(s) [${telefones}] no precatorio de ID ${id}`
			);
			while (telefones.length > 0) {
				let tel = telefones.pop();

				await mysqlPoolProd.query(QUERY_INSERT_TELEFONE, [
					id,
					tel,
					moment().utc().format('YYYY-MM-DD HH:mm:ss'),
				]);
			}
		} else {
			console.log(`Sem telefones para inserir.`);
		}
	}
}

await autobotTRF3(prod.default, prod.default);
process.exit(0);

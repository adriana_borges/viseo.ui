const Moment = require('moment');

const Fs = require('fs');

const config = require('./config.json');
const Sequelize = require('sequelize');

const sequelize = new Sequelize(config.mysql.database, config.mysql.user, config.mysql.password, {
    host: config.mysql.server,
    dialect: 'mysql'
});

const {
    Storage
} = require('@google-cloud/storage');

sequelize
.authenticate()
.then(() => {
    console.log('Conectado ao banco de dados.');
})
.catch(err => {
    console.error('Erro ao conectar ao banco de dados: ', err);
});

getExcel();

async function getExcel() {

    if (!process.env.LIMIT) {
        console.log('Preencha os parametros LIMIT=50 VALUE_MIN=70000.00 VALUE_MAX=5000000.00 ANO=2021 node excel.js');
        return process.exit(1);
    }

    if (!process.env.VALUE_MIN) {
        console.log('Preencha os parametros LIMIT=50 VALUE_MIN=70000.00 VALUE_MAX=5000000.00 ANO=2021 node excel.js');
        return process.exit(1);
    }

    if (!process.env.VALUE_MAX) {
        console.log('Preencha os parametros LIMIT=50 VALUE_MIN=70000.00 VALUE_MAX=5000000.00 ANO=2021 node excel.js');
        return process.exit(1);
    }

    if (!process.env.ANO) {
        console.log('Preencha os parametros LIMIT=50 VALUE_MIN=70000.00 VALUE_MAX=5000000.00 ANO=2021 node excel.js');
        return process.exit(1);
    }

    const results = await sequelize.query(
        `
        SELECT * FROM REQUISICOES 
        WHERE STATUS = 0 AND VALOR_INSCRITO_PROPOSTA >= ? AND VALOR_INSCRITO_PROPOSTA <= ?
        AND MES_ANO_PROPOSTA = '?-01-01'
        LIMIT ?`,
        {
            raw: true,
            replacements: [
                Number(process.env.VALUE_MIN),
                Number(process.env.VALUE_MAX),
                Number(process.env.ANO),
                Number(process.env.LIMIT)
            ],
            type: Sequelize.QueryTypes.SELECT
        }
    );

    if (results.length < 1) {
        console.log('Nenhum registro encontrado, mude os filtros.');
        return process.exit(1);
    }

    console.log('Importando o total de: ', results.length);

    var xl = require('excel4node');

    // Create a new instance of a Workbook class
    var wb = new xl.Workbook();

    // Add Worksheets to the workbook
    var ws = wb.addWorksheet('Sheet 1');
    var ws2 = wb.addWorksheet('Sheet 2');

    // Create a reusable style
    var style = wb.createStyle({
        alignment: {
            justifyLastLine: true,
        },
        font: {
            bold: true,
            color: '#000000',
            size: 12,
        },
        fill: { // §18.8.20 fill (Fill)
            type: 'pattern',
            patternType: 'solid',
            fgColor: '#CCCCCC'
        },
        numberFormat: '$#.##0,00; ($#.##0,00); -',
    });

    ws.cell(1, 1)
    .string('Cedente')
    .style(style);
    ws.cell(1, 2)
    .string('CPF/CNPJ')
    .style(style);
    ws.cell(1, 3)
    .string('Valor')
    .style(style);
    ws.cell(1, 4)
    .string('% Desconto')
    .style(style);
    ws.cell(1, 5)
    .string('VL. Líquido')
    .style(style);
    ws.cell(1, 6)
    .string('% Proposto')
    .style(style);
    ws.cell(1, 7)
    .string('VL. PROPOSTA')
    .style(style);
    ws.cell(1, 8)
    .string('Recurso')
    .style(style);
    ws.cell(1, 9)
    .string('Posicionamento')
    .style(style);
    ws.cell(1, 10)
    .string('Status')
    .style(style);

    for (let i = 11; i < 41; ++i) {
        ws.cell(1, i)
        .string(`Telefone ${i-10}`)
        .style(style);
    }

    ws.cell(1, 41)
    .string('UF da Vara')
    .style(style);
    ws.cell(1, 42)
    .string('Tribunal')
    .style(style);
    ws.cell(1, 43)
    .string('Ano do Precatório')
    .style(style);
    ws.cell(1, 44)
    .string('Precatório')
    .style(style);
    ws.cell(1, 45)
    .string('Processo')
    .style(style);
    ws.cell(1, 46)
    .string('Ofício Requisitório')
    .style(style);
    ws.cell(1, 47)
    .string('Processo Originário')
    .style(style);
    ws.cell(1, 48)
    .string('Natureza')
    .style(style);
    ws.cell(1, 49)
    .string('Requerido (Réu)')
    .style(style);
    ws.cell(1, 50)
    .string('Tipo de Causa')
    .style(style);
    ws.cell(1, 51)
    .string('Juízo Origem')
    .style(style);
    ws.cell(1, 52)
    .string('Cidade')
    .style(style);
    ws.cell(1, 53)
    .string('Estado')
    .style(style);
    ws.cell(1, 54)
    .string('Advogado')
    .style(style);
    ws.cell(1, 55)
    .string('OAB')
    .style(style);

    results.forEach((item , index) => {

        const row = index + 2;

        ws.cell(row, 1)
        .string(item.requerentes);

        ws.cell(row, 2)
        .string(item.numero);

        let valor = Number(item.valor_inscrito_proposta);

        if (Number(item.valor_inscrito_proposta) == 0) {
            valor = Number(item.valor_solicitado);
        }

        ws.cell(row, 3)
            .number(valor);

        ws.cell(row, 4)
        .string('33%');

        ws.cell(row, 5)
        .formula(`C${row}-(C${row}*D${row})`);

        const year = Moment().year();
        const yearPrec = item.mes_ano_proposta.split('-')[0];

        const valPerc = (Number(yearPrec) - Number(year));

        ws.cell(row, 6)
        .string((valPerc === 1) ? '63%' : '53%');

        ws.cell(row, 7)
        .formula(`E${row}*F${row}`);

        ws.cell(row, 8)
        .string('Cons001');

        ws.cell(row, 9)
        .string('');

        ws.cell(row, 10)
        .string('');

        for (let i = 11; i < 41; ++i) {
            ws.cell(row, i)
            .string('');
        }

        const uf_vara = item.juizo_origem.split(' ');

        ws.cell(row, 41)
        .string(uf_vara[uf_vara.length-1]);

        ws.cell(row, 42)
        .string('TRF3');

        ws.cell(row, 43)
        .string(item.mes_ano_proposta.split('-')[0]);

        ws.cell(row, 44)
        .string(item.numero);

        ws.cell(row, 45)
        .string(item.processos_originarios);

        ws.cell(row, 46)
        .string(item.oficio_requisitorio);

        ws.cell(row, 47)
        .string(item.processos_originarios);

        ws.cell(row, 48)
        .string(item.natureza);

        ws.cell(row, 49)
        .string(item.requerido);

        ws.cell(row, 50)
        .string('');

        ws.cell(row, 51)
        .string(item.juizo_origem);

        ws.cell(row, 52)
        .string(' ');

        ws.cell(row, 53)
        .string(' ');

        ws.cell(row, 54)
        .string(item.advogado);

        ws.cell(row, 55)
        .string(' ');
    });

    const fileName = 'excel-' + Moment().format('DD-MM-YYYY') + '.1.xlsx';

    wb.write(fileName, async function(err, stats) {

        const params = {
            projectId: '103501480665',
            keyFilename: './direct-obelisk-256018-b4c89076e0f3.json',
            fileName: fileName,
            bucket: 'isinvest',
            predefinedAcl: 'publicRead',
            contentType: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        };
    
        const documentos = results.map((item) => {
            return item.documento;
        });

        url = await sendFile(params, documentos);
    });
}

async function sendFile(options, documentos) {

    return new Promise(async (resolve, reject) => {

        const gcs = new Storage({
            projectId: options.projectId,
            keyFilename: options.keyFilename
        });

        const bucket = gcs.bucket(options.bucket);
        
        bucket.upload(options.fileName, async function(err, file) {

            if (err) throw new Error(err);

            await gcs
            .bucket(options.bucket)
            .file(options.fileName)
            .makePublic();

            const sql = `
                UPDATE requisicoes SET STATUS = 1 WHERE DOCUMENTO IN (?)
            `;

            sequelize.query(
                sql,
                {
                    raw: true,
                    replacements: [
                        documentos
                    ],
                    type: Sequelize.QueryTypes.INSERT
                }
            )
            .then(async result => {

                console.log('Finalizou a exportação!');
                console.log('URL: https://storage.googleapis.com/isinvest/' + options.fileName);
                // Fs.unlinkSync(options.fileName);
            });
        });
    });
}